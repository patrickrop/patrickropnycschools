package com.example.a20220718_patrickrop_nycschools.model

import com.example.a20220718_patrickrop_nycschools.model.remote.RetrofitClient
import com.example.a20220718_patrickrop_nycschools.model.remote.RetrofitService

class Repository {

    val retrofitService:RetrofitService = RetrofitClient.getService()

    suspend fun schoolList() = retrofitService.getSchoolList()

    suspend fun schoolItems(id:String) = retrofitService.getSchoolItems(id)
}