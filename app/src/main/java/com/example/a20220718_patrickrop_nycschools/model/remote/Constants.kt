package com.example.a20220718_patrickrop_nycschools.model.remote

object Constants {
    const val BASE_URL = "https://data.cityofnewyork.us/resource/"
}