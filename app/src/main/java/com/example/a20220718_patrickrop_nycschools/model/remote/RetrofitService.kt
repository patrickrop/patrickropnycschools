package com.example.a20220718_patrickrop_nycschools.model.remote

import com.example.a20220718_patrickrop_nycschools.model.remote.dataclass.ItemResponse
import com.example.a20220718_patrickrop_nycschools.model.remote.dataclass.ItemResponseItem
import com.example.a20220718_patrickrop_nycschools.model.remote.dataclass.SchoolListResponse
import com.example.a20220718_patrickrop_nycschools.model.remote.dataclass.SchoolListResponseItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {

    @GET("s3k6-pzi2")
    suspend fun getSchoolList():Response<List<SchoolListResponseItem>>

    @GET("f9bf-2cp4")
    suspend fun getSchoolItems(@Query("dbn")dbnId:String):Response<List<ItemResponseItem>>
}