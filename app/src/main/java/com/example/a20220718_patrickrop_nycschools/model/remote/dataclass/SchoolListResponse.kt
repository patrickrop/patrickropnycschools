package com.example.a20220718_patrickrop_nycschools.model.remote.dataclass

class SchoolListResponse : ArrayList<SchoolListResponseItem>()