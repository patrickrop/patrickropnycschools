package com.example.a20220718_patrickrop_nycschools.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.a20220718_patrickrop_nycschools.R
import com.example.a20220718_patrickrop_nycschools.databinding.ActivityMainBinding
import com.example.a20220718_patrickrop_nycschools.model.Repository
import com.example.a20220718_patrickrop_nycschools.view.adapter.SchoolAdapter
import com.example.a20220718_patrickrop_nycschools.viewmodel.SchoolViewModel
import com.example.a20220718_patrickrop_nycschools.viewmodel.factory.VMFactory

class MainActivity : AppCompatActivity(), SchoolAdapter.OnItemClickDbnListener {
    private lateinit var binding: ActivityMainBinding
    private lateinit var schoolViewModel: SchoolViewModel
    private lateinit var factory: VMFactory
    private lateinit var repository: Repository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setUpViewModel()
        setUpObserver()
    }

    private fun setUpViewModel() {

        repository = Repository()
        factory = VMFactory(repository)
        schoolViewModel = ViewModelProvider(this, factory)[SchoolViewModel::class.java]
    }

    private fun setUpObserver() {
        schoolViewModel.listResult.observe(this, Observer {

            binding.recyclerview.adapter = SchoolAdapter(this, it)

        })
    }

    override fun passDbn(id: String) {
        val intent = Intent(this,SchoolDetailsActivity::class.java)
        intent.putExtra("data",id)
        startActivity(intent)
    }
}