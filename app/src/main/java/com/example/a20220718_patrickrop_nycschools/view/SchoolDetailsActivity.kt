package com.example.a20220718_patrickrop_nycschools.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.a20220718_patrickrop_nycschools.R
import com.example.a20220718_patrickrop_nycschools.databinding.ActivitySchoolDetailsBinding
import com.example.a20220718_patrickrop_nycschools.model.Repository
import com.example.a20220718_patrickrop_nycschools.viewmodel.SchoolViewModel
import com.example.a20220718_patrickrop_nycschools.viewmodel.factory.VMFactory

class SchoolDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySchoolDetailsBinding
    private lateinit var schoolViewModel: SchoolViewModel
    private lateinit var factory: VMFactory
    private lateinit var repository: Repository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_school_details)

        setupViewModel()
        val id = intent.getStringExtra("data")
        if (id != null) {
            schoolViewModel.getSchoolItem(id)
        }
    }

    private fun setupViewModel() {
        repository = Repository()
        factory = VMFactory(repository)
        schoolViewModel = ViewModelProvider(this, factory)[SchoolViewModel::class.java]
        binding.viewmodel = schoolViewModel
    }
}