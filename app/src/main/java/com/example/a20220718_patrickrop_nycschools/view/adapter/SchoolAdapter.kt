package com.example.a20220718_patrickrop_nycschools.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220718_patrickrop_nycschools.R
import com.example.a20220718_patrickrop_nycschools.databinding.SchoolListBinding
import com.example.a20220718_patrickrop_nycschools.model.remote.dataclass.SchoolListResponseItem

class SchoolAdapter(val onItemClickDbnListener: OnItemClickDbnListener, private val listSchools: List<SchoolListResponseItem>) :
    RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder>() {
    private lateinit var binding: SchoolListBinding


    override fun getItemCount() = listSchools.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.school_list,
            parent,
            false
        )
        return (SchoolViewHolder(binding))
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bind(listSchools[position])
        holder.itemView.setOnClickListener {
            onItemClickDbnListener.passDbn(listSchools[position].dbn)
        }
    }

    inner class SchoolViewHolder(binding: SchoolListBinding) :
        RecyclerView.ViewHolder(binding.root) {
            fun bind(schoolListResponseItem: SchoolListResponseItem){
                binding.data = schoolListResponseItem
            }
    }

    interface OnItemClickDbnListener{
        fun passDbn(id:String)
    }
}