package com.example.a20220718_patrickrop_nycschools.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220718_patrickrop_nycschools.model.Repository
import com.example.a20220718_patrickrop_nycschools.model.remote.dataclass.ItemResponseItem
import com.example.a20220718_patrickrop_nycschools.model.remote.dataclass.SchoolListResponseItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SchoolViewModel(private val repository: Repository) : ViewModel() {

    val listResult = MutableLiveData<List<SchoolListResponseItem>>()
    val itemResult = MutableLiveData<ItemResponseItem>()
    val error = MutableLiveData<String>()

    fun getSchoolList() {
        viewModelScope.launch(Dispatchers.IO) {
            val listResponse = repository.schoolList()

            if (listResponse != null && listResponse.isSuccessful) {
                listResult.postValue(listResponse.body())
            } else {
                error.postValue("No Data Found!")
            }
        }
    }

    fun getSchoolItem(id: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val itemResponse = repository.schoolItems(id)

            if (itemResponse != null && itemResponse.isSuccessful) {
                itemResult.postValue(itemResponse.body()?.get(0))
            } else {
                error.postValue("No Data Found!")
            }

        }
    }
}